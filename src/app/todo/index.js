import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import { TableTodo } from "../../components/table"
import { index } from "../../services/TodoApi"
import { AiOutlinePlusCircle } from "react-icons/ai"
import {CreateModal} from "../../components/modal"
import {SwipeableTemporaryDrawer} from "../../components/sidebar"
import "./index.css";
import Button from '@material-ui/core/Button';

export const Todo = () => {
    const [state, setState] = useState({
        created: "",
        todos: [],
        allTodos: [],
        handleShow: false
    });

    useEffect(() => {
        getRows();
    }, [])

    const getRows = async () => {
        let response = await index();
        let todos = response.data.todos
        setState({ ...state, todos, allTodos: todos })
    }

    useEffect(() => {
        if (state.created !== "") {
            let todos = state.allTodos.filter(todo => todo.created === state.created)
            setState({ ...state, todos })
        }
    }, [state.created])

    const handleOnchange = (e) => {
        setState({ ...state, [e.target.name]: e.target.value });
        console.log(state);
    }

    const handleButtonCreate =()=>
    {
        setState({...state,handleShow: true})
    }

    return (
        <div className="container-fluid p-5 ">
            <h2 className="font-weight-bold">My Tasks</h2>
            <div className="container-fluid bg-white  fill card">
                <div className="row justify-content-between">
                    <div >
                        <h5 className="font-weight-bold mt-3 pl-3">Tasks</h5>
                    </div>
                    <div className="row">
                        <div className="align-self-end">
                            <TextField
                                type="date"
                                id="created"
                                name="created"
                                defaultValue="Default Value"
                                placeholder="Created"
                                InputLabelProps={{
                                    shrink: true
                                }}
                                margin="dense"
                                variant="outlined"
                                onChange={(e) => (handleOnchange(e))}
                            />
                        </div>
                        <div className="border-end h-75 mx-3">
                            <span
                            onClick={()=>handleButtonCreate()} 
                            >
                                <Button className="text-primary font-weight-bold">
                                    <AiOutlinePlusCircle />
                                    add Task
                                </Button>
                                
                            </span>
                        </div>
                    </div>
                </div>

                <TableTodo
                    rows={state.todos}

                />
            {state.handleShow && <CreateModal
             state = {state}
             setState = {setState}
            />}
                <SwipeableTemporaryDrawer/>
            </div>

        </div>

    )

}